﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class SatelliteGenerator
    {

        public SatelliteGenerator() { }

        public Satellite Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            Satellite satellite = new Satellite();

            satellite.Type = GetSatelliteType();
            satellite.CountryOfOrigin = GetCountryString();
            satellite.Orbit = GetOrbit();

            if (satellite.Type == SatelliteType.Station)
            {
                satellite.IsHabitable = true;
                satellite.IsDockable = true;
            }
            else
            {
                satellite.IsHabitable = random.NextDouble() < 0.3;
                satellite.IsDockable = random.NextDouble() < 0.4;
            }

            return satellite;
        }

        private SatelliteType GetSatelliteType()
        {
            RandomSingleton random = RandomSingleton.Instance();

            return (SatelliteType)random.Next((int)SatelliteType.Count);
        }

        private OrbitType GetOrbit()
        {
            RandomSingleton random = RandomSingleton.Instance();

            if (random.NextDouble() < 0.2)
                return OrbitType.GraveyardOrbit;

            return (OrbitType)random.Next((int)OrbitType.Count - 1);
        }

        private String GetCountryString()
        {
            RandomSingleton random = RandomSingleton.Instance();

            return _countries[random.Next()];
        }

        private String[] _countries = { "US", "UK", "JP", "CH", "DE", "FR", "RU", "CA", "IN", "NK" };
    }
}
